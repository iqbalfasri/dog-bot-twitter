const Twit = require('twit');
const CronJob = require('cron').CronJob;
const request = require('request');

const config = {
  consumer_key: '<consuer_key>',
  consumer_secret: '<consumer_secret>',
  access_token: '<access_token>',
  access_token_secret: '<access_token_secret>'
};

// Configure Twitter API
const Twitter = new Twit(config);

let url = 'https://us-central1-bot-anjing.cloudfunctions.net/app/list-kata';

// Fungsi untuk acak urutan kosakata
function acak(kata) {
  return kata[Math.floor(Math.random() * kata.length)];
}

// Fungsi Tweet dengan kata-kata yang sudah dipecah
function tweet() {
  request(url, (err, res, body) => {
    Twitter.post('statuses/update', {
      status: `${acak(JSON.parse(body))} anjing`
    }, (error, data, response) => {
      console.log(JSON.stringify(data))
    });
  })
}

// Post tweet per-30 menit
const autoTweet = new CronJob({
  cronTime: '*/30 * * * *',
  onTick: () => {
    tweet();
  },
  start: true,
  timeZone: 'Asia/Jakarta'
});

module.exports = autoTweet;