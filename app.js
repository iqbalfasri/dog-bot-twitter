let express = require('express');
let app = express();

// Import Bot
const autoTweet = require('./bot');

app.get('/', (req, res) => {
  // Jalankan bot
  autoTweet.start()
  res.json('Halo :)')
});

app.listen(1999, () => {
  console.log('Running on port 1999')
})